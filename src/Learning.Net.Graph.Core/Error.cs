namespace Learning.Net.Graph.Core;

public struct Error
{
    private readonly string? _message;

    private readonly Exception? _exception;

    public string Message => _message ?? (_exception?.Message ?? string.Empty);

    public Exception? Exception => _exception;
    public Error(string message)
    {
        _message = message;
        _exception = null;
    }

    public Error(Exception exception)
    {
        _message = null;
        _exception = exception;
    }
}