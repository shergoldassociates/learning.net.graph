﻿using System;

using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.Extensions.Logging;

using Serilog;

using Xunit.Abstractions;

namespace Learning.Net.Graph.Testing;

public abstract class Tests
{
    private readonly ITestOutputHelper _testOutputHelper;
    
    protected Tests(ITestOutputHelper testOutputHelper)
    {
        _testOutputHelper = testOutputHelper;
    }
    
    protected IServiceProvider CreateServiceProvider(IServiceCollection? services = null)
    {
        var logger = new LoggerConfiguration()
            .WriteTo.Console()
            .WriteTo.TestOutput(_testOutputHelper)
            .CreateLogger();
        
        var serviceCollection = new ServiceCollection();
        
        serviceCollection.AddLogging(builder =>
        {
            builder.ClearProviders();
            builder.AddSerilog(logger, dispose: true);
        });

        if (services is not null)
        {
            serviceCollection.Add(services);
        }
        
        return serviceCollection.BuildServiceProvider();
    }
}