using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

using Xunit;
using Xunit.Abstractions;

namespace Learning.Net.Graph.Core.Tests;

public class ErrorTests : Testing.Tests
{
    private readonly ILogger _logger;
    
    public ErrorTests(ITestOutputHelper testOutputHelper) : base(testOutputHelper)
    {
        _logger = CreateServiceProvider().GetRequiredService<ILogger<ErrorTests>>();
    }
    
    [Fact]
    public void CanConstructWithMessageAndNoException()
    {
        _logger.LogInformation($"Running {nameof(CanConstructWithMessageAndNoException)}");
        
        var sut = new Error("an error occured");
        Assert.Equal("an error occured", sut.Message);
        Assert.Null(sut.Exception);
    }

}