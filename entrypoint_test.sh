#!/bin/sh -x

# exit if any command fails
set -e

mkdir -p /tmp/output

dotnet test --no-restore /p:CollectCoverage=true /p:CoverletOutputFormat=opencover /p:CoverletOutput=/tmp/coverage.xml src/Learning.Net.Graph.sln 

#cat /tmp/coverage.xml 
#codecov -t ${CODECOV_TOKEN} -f /tmp/coverage.xml 