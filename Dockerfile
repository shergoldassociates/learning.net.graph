FROM mcr.microsoft.com/dotnet/sdk:7.0 AS build
WORKDIR /app

COPY src src

RUN dotnet restore src/Learning.Net.Graph.sln

RUN dotnet build src/Learning.Net.Graph.sln


FROM build AS test

#RUN mkdir tools
#WORKDIR /tools
# RUN curl -Os https://uploader.codecov.io/latest/linux/codecov
# RUN chmod +x codecov
# ENV PATH="/tools:$PATH"

WORKDIR /app

COPY --chmod=0755 entrypoint_test.sh ./
ENTRYPOINT [ "/app/entrypoint_test.sh" ]

